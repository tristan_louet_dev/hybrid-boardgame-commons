Afin de faire communiquer les tablettes, il faut exécuter un Master server et un Facilitator server.
A la date du projet, les sources de ces serveurs sont disponibles sur le site de Unity.
Une version déboguée, compilable sous Linux et utilisée durant le projet peut être fournie par Tristan Louet.

Ceci étant, une instance de chacun de ces serveurs devrait déjà être en ligne à l'adresse ud.natsirtt.com, permettant de facilement lancer une partie sans s'occuper de cela.
Si ud.natsirtt.com de répond pas, il est possible de soit mettre en place ses propres serveurs, soit contacter tristan.louet.dev at gmail dot com afin que les serveurs soient remis en ligne dès que possible.