﻿using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts
{
    public class Singleton<T> : UndergroundBehavior where T : UndergroundBehavior
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                _instance = FindObjectOfType<T>();
                if (_instance == null)
                {
                    Debug.LogError("Singleton <" + typeof(T) + "> does not exists in the hierarchy.");
                }
                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
            }
            else
            {
                if (!Application.isEditor)
                {
                    Debug.LogError(this + " You have two singletons of the same type in the hierarchy");
                    //       Destroy(gameObject);
                }
            }
        }
    }
}
