﻿using System;
using System.Collections;
using Assets.commons.Scripts.GameStates;
using UnityEngine;

namespace Assets.commons.Scripts.Managers
{
    /// <summary>
    /// Final state machine managing the game.
    /// </summary>
    public class GameManager : Singleton<GameManager>
    {
        /// <summary>
        /// If the game manager switch to WaitNextFrameState between two states.
        /// </summary>
        [SerializeField]
        private bool _waitNextFrame;

        /// <summary>
        /// Current game state.
        /// </summary>
        private IState _state;

        /// <summary>
        /// Get/Set the current game state.
        /// </summary>
        public IState State
        {
            get { return _state; }
            set
            {
                // Pause message queue to stack incomming RPC.
                UnityEngine.Network.isMessageQueueRunning = false;
                // Switch state.
                Debug.Log(value);
                if (_state != null)
                {
                    _state.OnDeactivated();
                }
                if (value != null)
                {
                    if (_waitNextFrame && !(_state is WaitNextFrameState))
                    {
                        value = new WaitNextFrameState(value);
                    }
                    _state = value;
                    value.Paused = Paused;
                    value.OnActivated();
                }
                else
                {
                    _state = null;
                }
                // Unpause message queue.
                UnityEngine.Network.isMessageQueueRunning = true;
            }
        }

        /// <summary>
        /// If the game is paused.
        /// </summary>
        private bool _paused;

        /// <summary>
        /// Get/Set wether the game is paused.
        /// </summary>
        public bool Paused
        {
            get { return _paused; }
            private set
            {
                if (value == _paused) return;
                if (_state != null)
                {
                    _state.Paused = value;
                    if (value)
                    {
                        _state.OnPaused();
                    }
                    else
                    {
                        _state.OnUnpaused();
                    }
                }
                _paused = value;
            }
        }

        /// <summary>
        /// Pause the game while the given coroutine is executed.
        /// </summary>
        /// <param name="function">Coroutine to execute.</param>
        /// <param name="callback">Callback.</param>
        public void PauseToExecute(IEnumerator function, Action callback = null)
        {
            // Pause the game.
            Paused = true;
            // Execute the coroutine.
            StartCoroutine(ExecuteAndCall(function, () =>
            {
                // Unpause the game.
                Paused = false;
                if (callback != null)
                {
                    callback();
                }
            }));
        }

        /// <summary>
        /// Execute a coroutine and call a function.
        /// </summary>
        /// <param name="function">Coroutine to execute.</param>
        /// <param name="callback">Callback.</param>
        /// <returns></returns>
        public IEnumerator ExecuteAndCall(IEnumerator function, Action callback = null)
        {
            yield return StartCoroutine(function);
            if (callback != null)
            {
                callback();
            }
        }

        /// <summary>
        /// Number of completed turns.
        /// </summary>
        public int TurnCount { get; set; }

        /// <summary>
        /// Initialize the game state.
        /// </summary>
        protected virtual void Start()
        {
        }

        /// <summary>
        /// Update the game state.
        /// </summary>
        protected virtual void Update()
        {
            if (_state != null && !Paused)
            {
                _state.OnUpdated();
            }
        }

        /// <summary>
        /// Draw GUI.
        /// </summary>
        protected virtual void OnGUI()
        {
            if (_state != null)
            {
                _state.OnGUI();
            }
        }

        /// <summary>
        /// Draw gizmos.
        /// </summary>
        protected virtual void OnDrawGizmos()
        {
            if (_state != null)
            {
                _state.OnDrawGizmos();
            }
        }

        public static IEnumerator BlurDeath(GameObject toDestroy)
        {
            toDestroy.transform.position += new Vector3(0, 0, 100);
            yield return new WaitForSeconds(2);
            Destroy(toDestroy);
        }
    }
}
