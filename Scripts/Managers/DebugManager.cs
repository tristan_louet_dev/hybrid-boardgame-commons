﻿using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Managers
{
    public class DebugManager : UndergroundBehavior
    {
        public UITextList mLabel;
        private float _test;
    

        void OnEnable()
        {
            Application.RegisterLogCallback(HandleLog);
        }
        void OnDisable()
        {
            Application.RegisterLogCallback(null);
        }

        /**
     * Handle all Log in Unity (Debug.Log, Debug.LogError)
     */
        void HandleLog(string logString, string stackTrace, LogType type)
        {
            mLabel.Add(((type == LogType.Error) ? "[FF0000]" : "[AAAAAA]") + type.ToString() + ": " + logString);
        }
    }
}

