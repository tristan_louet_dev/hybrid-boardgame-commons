﻿namespace Assets.commons.Scripts.GameStates
{
    /// <summary>
    /// Enum for game states.
    /// </summary>
    public enum GameState
    {
        MainMenu,
        PreGame,
        PlayerOrderChoice,
        PlayerSpawn,
        PlayerTurn,
        PlayersTurnFinished,
        AITurn,
        AITurnFinished,
        EnemyTurn,
        Victory
    }
}
