﻿using Assets.commons.Scripts.Entities;

namespace Assets.commons.Scripts.GameStates
{
    /// <summary>
    /// Interface for game states.
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// Get the state name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get the current player or Character.None.
        /// </summary>
        Character Player { get; }

        /// <summary>
        /// Get/Set wether the game is paused or not.
        /// </summary>
        bool Paused { get; set; }

        /// <summary>
        /// Called when the game makes a transition to this state.
        /// </summary>
        void OnActivated();

        /// <summary>
        /// Called when the game is updated while in this state.
        /// </summary>
        void OnUpdated();

        /// <summary>
        /// Called when the game is paused.
        /// </summary>
        void OnPaused();

        /// <summary>
        /// Called when the game is unpaused.
        /// </summary>
        void OnUnpaused();

        /// <summary>
        /// Called when the game makes a transition from this state.
        /// </summary>
        void OnDeactivated();

        /// <summary>
        /// Called when the GUI is drawn while in this state.
        /// </summary>
        void OnGUI();

        /// <summary>
        /// Called when the gizmos are drawn while in this state.
        /// </summary>
        void OnDrawGizmos();
    }
}
