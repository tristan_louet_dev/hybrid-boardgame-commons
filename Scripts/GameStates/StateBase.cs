﻿using Assets.commons.Scripts.Entities;

namespace Assets.commons.Scripts.GameStates
{
    /// <summary>
    /// Base for game states.
    /// </summary>
    public abstract class StateBase :
#if STATE_MONO_BEHAVIOUR
        UndergroundBehavior,
#endif
        IState
    {
        public virtual string Name
        {
            get { return GetType().Name;}
        }

        public virtual Character Player
        {
            get { return Character.None; }
        }

        public bool Paused { get; set; }

        /// <summary>
        /// Wether the state is activated or not.
        /// </summary>
        protected bool Activated { get; set; }

        public virtual void OnActivated()
        {
            Activated = true;
        }

        public abstract void OnUpdated();

        public virtual void OnPaused()
        {
        }

        public virtual void OnUnpaused()
        {
        }

        public virtual void OnDeactivated()
        {
            Activated = false;
        }

        public virtual void OnGUI()
        {
        }

        public virtual void OnDrawGizmos()
        {
        }

#if STATE_MONO_BEHAVIOUR
        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }
#endif
    }
}
