﻿using Assets.commons.Scripts.Managers;

namespace Assets.commons.Scripts.GameStates
{
    public class WaitNextFrameState : StateBase
    {
        /// <summary>
        /// Next state.
        /// </summary>
        private readonly IState _next;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="next">Next state.</param>
        public WaitNextFrameState(IState next)
        {
            _next = next;
        }

        /// <summary>
        /// Switch to next state.
        /// </summary>
        public override void OnActivated()
        {
            base.OnActivated();
            if (!Paused)
            {
                GameManager.Instance.State = _next;
            }
        }

        /// <summary>
        /// Switch to next state.
        /// </summary>
        public override void OnUpdated()
        {
            if (!Paused)
            {
                GameManager.Instance.State = _next;
            }
        }
    }
}
