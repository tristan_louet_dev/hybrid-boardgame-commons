﻿using System;
using System.Collections.Generic;

namespace Assets.commons.Scripts.Cutscenes
{
    /// <summary>
    /// Cutscene parameters.
    /// </summary>
    [Serializable]
    public class CutsceneParams : ICutsceneParams
    {
        /// <summary>
        /// Scene identifier.
        /// </summary>
        private readonly SceneID _sceneID;

        /// <summary>
        /// UID of unit doing the action.
        /// </summary>
        private readonly int? _sourceUID;

        /// <summary>
        /// Targeted unit.
        /// </summary>
        private readonly IList<ITarget> _targets;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="parameters">Other parameters.</param>
        public CutsceneParams(ICutsceneParams parameters)
        {
            _sceneID = parameters.SceneID;
            _sourceUID = parameters.SourceUID;
            _targets = new List<ITarget>();
            if (parameters.Targets != null)
            {
                foreach (var target in parameters.Targets)
                {
                    _targets.Add(new Target(target));
                }
            }
        }

        public SceneID SceneID
        {
            get { return _sceneID; }
        }

        public int? SourceUID
        {
            get { return _sourceUID; }
        }

        public IList<ITarget> Targets
        {
            get { return _targets; }
        }

        public override string ToString()
        {
            var str = "CutsceneParams (sceneID=" + _sceneID + ", source=" + (!_sourceUID.HasValue ? "null" : SourceUID.Value.ToString()) +
                      ", targets={";
            var first = true;
            foreach (var target in _targets)
            {
                str = str + (first ? "" : ", ") + target.UID;
                first = false;
            }
            return str + "})";
        }
    }

    [Serializable]
    public class Target : ITarget
    {
        /// <summary>
        /// UID of targeted unit.
        /// </summary>
        private readonly int _uid;

        /// <summary>
        /// Has the unit been killed.
        /// </summary>
        private readonly bool _killed;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other target.</param>
        public Target(ITarget other)
        {
            _uid = other.UID;
            _killed = other.Killed;
        }

        public int UID
        {
            get { return _uid; }
        }

        public bool Killed
        {
            get { return _killed; }
        }
    }
}
