﻿namespace Assets.commons.Scripts.Cutscenes
{
    /// <summary>
    /// Cutscenes.
    /// </summary>
    public enum Cutscene
    {
        None,
        SkillJakeGoatCloseRange,
        SkillJakeGoatLongRange,
        SkillJakeBarrage,
        SkillAmySneakyStabRicochetCloseRange,
        SkillAmySneakyStabRicochetLongRange,
        SkillAmyAcrobatics,
        SkillDomShoot,
        SkillDomHealingGrenade,
        SkillDomStimulant
    }
}
