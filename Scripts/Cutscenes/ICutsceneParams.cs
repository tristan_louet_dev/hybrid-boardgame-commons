﻿using System.Collections.Generic;

namespace Assets.commons.Scripts.Cutscenes
{
    /// <summary>
    /// Interface for cutscene parameters.
    /// </summary>
    public interface ICutsceneParams
    {
        /// <summary>
        /// Scene identifier.
        /// </summary>
        SceneID SceneID { get; }

        /// <summary>
        /// UID of unit doing the action.
        /// </summary>
        int? SourceUID { get; }

        /// <summary>
        /// List of targeted units.
        /// </summary>
        IList<ITarget> Targets { get; }
    }

    /// <summary>
    /// Targeted unit.
    /// </summary>
    public interface ITarget
    {
        /// <summary>
        /// UID of the targeted unit.
        /// </summary>
        int UID { get; }

        /// <summary>
        /// Has the unit been killed.
        /// </summary>
        bool Killed { get; }
    };
}
