﻿using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts.Entities;

namespace Assets.commons.Scripts.Cutscenes
{
    /// <summary>
    /// Modifiable cutscene parameters.
    /// </summary>
    public class MutableCutsceneParams : ICutsceneParams
    {
        /// <summary>
        /// Scene identifier.
        /// </summary>
        private readonly SceneID _sceneID;

        /// <summary>
        /// Unit doing the action.
        /// </summary>
        private readonly IUnit _source;

        /// <summary>
        /// Targeted unit.
        /// </summary>
        private readonly IList<ITarget> _targets;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="sceneId"></param>
        /// <param name="target">Targeted unit.</param>
        /// <param name="killed">Has the unit been killed.</param>
        public MutableCutsceneParams(SceneID sceneId, IUnit target, bool killed = false) : this(sceneId, null, target, killed)
        {
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="sceneId"></param>
        /// <param name="source">Unit doing the action.</param>
        /// <param name="target">Targeted unit.</param>
        /// <param name="killed">Has the unit been killed.</param>
        public MutableCutsceneParams(SceneID sceneId, IUnit source, IUnit target, bool killed = false)
            : this(sceneId, source, new List<ITarget>() { new MutableTarget(target, killed) })
        {
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="sceneId"></param>
        /// <param name="targets">Targeted units.</param>
        public MutableCutsceneParams(SceneID sceneId, IEnumerable<IUnit> targets)
            : this(sceneId, null, targets.Select(target => new MutableTarget(target)).Cast<ITarget>().ToList())
        {
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="sceneId"></param>
        /// <param name="source">Unit doing the action.</param>
        /// <param name="targets">Targeted units.</param>
        public MutableCutsceneParams(SceneID sceneId, IUnit source, IList<ITarget> targets)
        {
            _sceneID = sceneId;
            _source = source;
            _targets = targets;
        }

        public SceneID SceneID
        {
            get { return _sceneID; }
        }

        public int? SourceUID
        {
            get
            {
                if (_source == null) return null;
                else return _source.UID;
            }
        }

        public IList<ITarget> Targets
        {
            get { return _targets.Select(target => new Target(target)).Cast<ITarget>().ToList(); }
        }
    }

    public class MutableTarget : ITarget
    {
        /// <summary>
        /// Targeted unit.
        /// </summary>
        private readonly IUnit _unit;

        /// <summary>
        /// Has the unit been killed.
        /// </summary>
        private readonly bool _killed;

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="unit">Targeted unit.</param>
        /// <param name="killed">Has the unit been killed.</param>
        public MutableTarget(IUnit unit, bool killed = false)
        {
            _unit = unit;
            _killed = killed;
        }

        public int UID
        {
            get { return _unit.UID; }
        }

        public bool Killed
        {
            get { return _killed; }
        }
    }
}
