﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Buffs {
    [Serializable]
    public class DamageReductionBuffStats : BuffStats, IDamageReductionBuffStats {
        [SerializeField]
        [Range(0f, 1f)]
        private float _damageReduction;

        public float DamageReduction
        {
            get { return _damageReduction; }
            set { _damageReduction = value; }
        }

        public DamageReductionBuffStats(IDamageReductionBuffStats other)
            : base(other)
        {
            _damageReduction = other.DamageReduction;
        }
    }
}
