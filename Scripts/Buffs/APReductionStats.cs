﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Buffs {
    [Serializable]
    public class APReductionStats : BuffStats, IAPReductionBuffStats {
        [SerializeField]
        private int _apReduction;

        public int APReduction
        {
            get { return _apReduction; }
            set { _apReduction = value; }
        }

        public APReductionStats(IAPReductionBuffStats other)
            : base(other)
        {
            _apReduction = other.APReduction;
        }
    }
}
