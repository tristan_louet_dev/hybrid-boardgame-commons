﻿namespace Assets.commons.Scripts.Buffs {
    public interface IDamagingBuffStats : IBuffStats {
        /// <summary>
        /// The number of damages applied on the target at each buff tick.
        /// </summary>
        int DamagesPerTick { get; }
    }
}
