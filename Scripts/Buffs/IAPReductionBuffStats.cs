﻿namespace Assets.commons.Scripts.Buffs {
    /// <summary>
    /// Stats for a buff reducting APs.
    /// </summary>
    public interface IAPReductionBuffStats : IBuffStats {
        /// <summary>
        /// The number of AP this buff will remove from its target.
        /// </summary>
        int APReduction { get; set; }
    }
}
