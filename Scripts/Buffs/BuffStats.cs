﻿using System;
using Assets.commons.Scripts.Entities;
using UnityEngine;

namespace Assets.commons.Scripts.Buffs {
    [Serializable]
    public class BuffStats : IBuffStats
    {
        [SerializeField]
        private ApplicationType _applicationType;
        public ApplicationType ApplicationType
        {
            get { return _applicationType; }
            set
            {
                _applicationType = value;
                if (value.IsTargetDependant())
                {
                    OriginalApplicationType = value;
                }
            }
        }

        [SerializeField]
        private bool _neverExpire;

        public ApplicationType OriginalApplicationType { get; set; }

        public bool NeverExpire
        {
            get { return _neverExpire; }
            set { _neverExpire = value; }
        }

        private bool _expired = false;
        // -1 is not a straightforward way to think of an expired Buff, but it is the way it is specified. cf game designers.
        public bool Expired {
            get { return _expired || !NeverExpire && (RemainingTicksNb == -1); }
            set { _expired = value; }
        }

        [SerializeField]
        private int _totalTicksNb;

        public int TotalTicksNb
        {
            get { return _totalTicksNb; }
            set { _totalTicksNb = value; }
        }

        public int RemainingTicksNb { get; set; }

        [SerializeField]
        private Status _statusApplied;

        public Status StatusApplied
        {
            get { return _statusApplied; }
        }
        public BuffStats(IBuffStats other)
        {
            NeverExpire = other.NeverExpire;
            _expired = other.Expired;
            _totalTicksNb = other.TotalTicksNb;
            RemainingTicksNb = other.RemainingTicksNb;
            ApplicationType = other.ApplicationType;
            OriginalApplicationType = other.OriginalApplicationType;
            _statusApplied = other.StatusApplied;
        }
    }
}
