﻿using Assets.commons.Scripts.Entities;

namespace Assets.commons.Scripts.Buffs {
    /// <summary>
    /// Specifies buff stats and utilities.
    /// </summary>
    public interface IBuffStats {
        /// <summary>
        /// Parametrizes at which key moment of the game this buff will be ticked.
        /// <remarks>The application type can change when a buff is applied to a tile. The original and true application type is still stored in <see cref="OriginalApplicationType"/></remarks>
        /// </summary>
        ApplicationType ApplicationType { get; set; }

        /// <summary>
        /// The true application type of this buff.
        /// </summary>
        ApplicationType OriginalApplicationType { get; set; }

        /// <summary>
        /// Indicates wether or not this buff can or cannot expire.
        /// </summary>
        bool NeverExpire { get; set; }

        /// <summary>
        /// If this effect is still activable (Tick can be called) or not.
        /// </summary>
        bool Expired { get; set; }

        /// <summary>
        /// The total number of time this buff will tick.
        /// </summary>
        int TotalTicksNb { get; }

        /// <summary>
        /// The number of time this buff has to tick before expiring.
        /// </summary>
        int RemainingTicksNb { get; set; }

        /// <summary>
        /// The status applied on the player by this buff.
        /// </summary>
        Status StatusApplied { get; }
    }
}
