﻿namespace Assets.commons.Scripts.Buffs {
    public interface IDamageReductionBuffStats : IBuffStats {
        /// <summary>
        /// Damage reduction percentage applid on incoming damages.
        /// </summary>
        float DamageReduction { get; set; }
    }
}
