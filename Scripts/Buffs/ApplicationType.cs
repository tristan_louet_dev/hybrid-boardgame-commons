﻿using Assets.commons.Scripts.Utils;

namespace Assets.commons.Scripts.Buffs {
    public enum ApplicationType {
        Never,
        OnTargetTurnStart,
        OnTargetTurnEnd,
        OnPlayersTurnStart,
        OnPlayersTurnEnd,
        OnEnemiesTurnStart,
        OnEnemiesTurnEnd,
    }

    public static class ApplicationTypeExtensions
    {
        public static bool IsTargetDependant(this ApplicationType type)
        {
            return type == ApplicationType.OnTargetTurnStart || type == ApplicationType.OnTargetTurnEnd;
        }

        public static ApplicationType TargetUndependantEquivalent(this ApplicationType type)
        {
            if (!type.IsTargetDependant())
            {
                return type;
            }

            switch (type)
            {
                case ApplicationType.OnTargetTurnStart:
                    //return buff.Target.Camp == Camp.Enemy ? ApplicationType.OnEnemiesTurnStart : ApplicationType.OnPlayersTurnStart;
                    return ApplicationType.OnEnemiesTurnStart;
                case ApplicationType.OnTargetTurnEnd:
                    //return buff.Target.Camp == Camp.Enemy ? ApplicationType.OnEnemiesTurnEnd : ApplicationType.OnPlayersTurnEnd;
                    return ApplicationType.OnEnemiesTurnEnd;
            }
            
            throw new ImpossibruExecutionFlowException();
        }
    }
}
