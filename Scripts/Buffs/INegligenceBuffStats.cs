﻿namespace Assets.commons.Scripts.Buffs {
    public interface INegligenceBuffStats : IBuffStats {
        /// <summary>
        /// The number of tiles accessibility the target gains.
        /// </summary>
        int MovementAugmentation { get; }
    }
}
