﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Buffs {
    [Serializable]
    public class DamagingBuffStats : BuffStats, IDamagingBuffStats {
        [SerializeField]
        private int _damagesPerTick;
        public int DamagesPerTick { get { return _damagesPerTick; } }

        public DamagingBuffStats(IDamagingBuffStats other) : base(other)
        {
            _damagesPerTick = other.DamagesPerTick;
        }
    }
}
