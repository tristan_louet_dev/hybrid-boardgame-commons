﻿using System;

namespace Assets.commons.Scripts.Buffs {
    [Serializable]
    public class NegligenceBuffStats : BuffStats, INegligenceBuffStats {
        public int MovementAugmentation { get; set; }

        public NegligenceBuffStats(INegligenceBuffStats other) : base(other)
        {
            MovementAugmentation = other.MovementAugmentation;
        }
    }
}
