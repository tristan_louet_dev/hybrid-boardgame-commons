﻿using System;
using System.Linq;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Audio.Utils;
using UnityEngine;
using System.Collections.Generic;

namespace Audio {
    public class AudioManager : UndergroundBehavior {
        #region Singleton
        private static AudioManager _instance;
        public static AudioManager Instance {
            get { return _instance ?? (_instance = FindObjectOfType<AudioManager>()); }
        }
        #endregion

        #region Available channels
        public enum Channel {
            Sfx,
            Voice,
            Ambient,
            Music,
        }

        private static IEnumerable<Channel> ChannelValues() {
            return (Channel[]) Enum.GetValues(typeof (Channel));
        }
        #endregion

        private bool _muted = false;
        private float _masterVolumeBackup;
        /// <summary>
        /// Whether or not all sounds managed by this AudioManager are muted.
        /// </summary>
        public bool Muted {
            get { return _muted; }
            set {
                if (_muted == value) {
                    return;
                }
                _muted = value;
                if (_muted) {
                    _masterVolumeBackup = MasterVolume;
                    _masterVolume = 0f;
                } else {
                    _masterVolume = _masterVolumeBackup;
                }
            }
        }

        [Range(0f, 1f)] public float _masterVolume = 1f;
        /// <summary>
        /// The master sound volume of all channels.
        /// <remarks>You should use <see cref="Muted"/> if you want to mute/unmute all channels.</remarks>
        /// </summary>
        public float MasterVolume {
            get { return _masterVolume; }
#pragma warning disable 618
            set { SetMasterVolume(value); }
#pragma warning restore 618
        }

        /// <summary>
        /// Ambience sounds audio sources pool size.
        /// </summary>
        [Range(1, 100)]
        public int m_ambientPoolSize = 25;
        /// <summary>
        /// Musics audio sources pool size.
        /// </summary>
        [Range(1, 100)]
        public int m_musicPoolSize = 4;
        /// <summary>
        /// SFXs sounds audio sources pool size.
        /// </summary>
        [Range(1, 100)]
        public int m_sfxPoolSize = 100;
        /// <summary>
        /// Voices sounds audio sources pool size.
        /// </summary>
        [Range(1, 100)]
        public int m_voicePoolSize = 30;

        /// <summary>
        /// The speed in seconds at which fading sounds will fade.
        /// </summary>
        public float m_fadeInAndOutSpeed = 1f;

        private Dictionary<Channel, float> _channelsVolumeBackup; 
        private Dictionary<Channel, float> _channelsVolume;
        /// <summary>
        /// Each sound channel's volume.
        /// </summary>
        public Dictionary<Channel, float> ChannelsVolume {
            get { return new Dictionary<Channel, float>(_channelsVolume); }
        }

        private Dictionary<Channel, bool> ChannelMuted { get; set; }

        /// <summary>
        /// Channels volume computed with MasterVolume.
        /// </summary>
        public Dictionary<Channel, float> ActualChannelsVolume {
            get {
                var channelsVolume = ChannelsVolume;
                return ChannelValues().ToDictionary(channel => channel, channel => channelsVolume[channel] * MasterVolume);
            }
        } 

        private Dictionary<Channel, Pool<AudioSourceWrapper>> _channelsPool;
        private Transform _audioSourcesParent;
        private Dictionary<long, AudioSourceWrapper> _currentlyUsedSources; 
        private long _nextId = 1;

        void Awake() {
            DontDestroyOnLoad(gameObject);
            _audioSourcesParent = new GameObject().transform;
            _audioSourcesParent.name = "Pooled audio sources";
            _audioSourcesParent.parent = transform;
            _channelsVolumeBackup = new Dictionary<Channel, float>();
            _channelsVolume = new Dictionary<Channel, float>();
            ChannelMuted = new Dictionary<Channel, bool>();
            foreach (var channel in ChannelValues()) {
                _channelsVolume.Add(channel, 1f);
                _channelsVolumeBackup.Add(channel, 1f);
                ChannelMuted.Add(channel, false);
            }

            //Could be more generic, with an editor script replacing the 4 hardcoded
            //pool size variables by dynamically checking the enum.
            _channelsPool = new Dictionary<Channel, Pool<AudioSourceWrapper>> {
                {Channel.Ambient, new Pool<AudioSourceWrapper>(InstantiateAudioSource, m_ambientPoolSize, m_ambientPoolSize)},
                {Channel.Music, new Pool<AudioSourceWrapper>(InstantiateAudioSource, m_musicPoolSize, m_musicPoolSize)},
                {Channel.Sfx, new Pool<AudioSourceWrapper>(InstantiateAudioSource, m_sfxPoolSize, m_sfxPoolSize)},
                {Channel.Voice, new Pool<AudioSourceWrapper>(InstantiateAudioSource, m_voicePoolSize, m_voicePoolSize)}
            };
            _currentlyUsedSources = new Dictionary<long, AudioSourceWrapper>();
        }

        void Update()
        {
            if (Muted) {
                _masterVolume = 0f;
            }
            foreach (var channel in ChannelMuted.Keys.Where(k => ChannelMuted[k]))
            {
                _channelsVolume[channel] = 0f;
            }
        }

        private AudioSourceWrapper InstantiateAudioSource() {
            var audioSourceObj = new GameObject {name = "Audio source wrapper"};
            audioSourceObj.transform.parent = _audioSourcesParent;
            return audioSourceObj.AddComponent<AudioSourceWrapper>();
        }

        /// <summary>
        /// Sets the volume of a specific channel.
        /// <remarks>The voulme will be clamped between 0 and 1.</remarks>
        /// </summary>
        /// <param name="pChannel">The channel to sets the volume to.</param>
        /// <param name="pVolume">The new channel's volume.</param>
        public void SetChannelVolume(Channel pChannel, float pVolume) {
            _channelsVolume[pChannel] = Mathf.Clamp(pVolume, 0f, 1f);
            _channelsVolumeBackup[pChannel] = _channelsVolume[pChannel];
        }

        /// <summary>
        /// Set whether or not a channel is muted.
        /// </summary>
        /// <param name="pChannel">The channel to set the muted state to.</param>
        /// <param name="pMuted">Whether or not the channel is muted.</param>
        public void SetChannelMuted(Channel pChannel, bool pMuted)
        {
            ChannelMuted[pChannel] = pMuted;
            _channelsVolume[pChannel] = pMuted ? 0f : _channelsVolumeBackup[pChannel];
        }

        /// <summary>
        /// Returns whether or not a channel is muted.
        /// </summary>
        /// <param name="pChannel">The channel to check.</param>
        /// <returns></returns>
        public bool IsChannelMuted(Channel pChannel)
        {
            return ChannelMuted[pChannel];
        }
        
        /// <summary>
        /// Sets the Master volume.
        /// </summary>
        /// <param name="pVolume">The new volume.</param>
        [Obsolete("Use the property MasterVolume instead.", false)]
        public void SetMasterVolume(float pVolume) {
            _masterVolumeBackup = Mathf.Clamp(pVolume, 0f, 1f);
            if (!Muted) {
                _masterVolume = _masterVolumeBackup;
            }
        }

        /// <summary>
        /// Changes the pitch of a currently playing 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pPitch"></param>
        public void SetAudioDataPitch(long pId, float pPitch) {
            try {
                _currentlyUsedSources[pId].CurrentAudioData.m_pitch = Mathf.Clamp(pPitch, -3f, 3f);
            }
            catch (KeyNotFoundException) {
                Debug.LogWarning("You tried to set the pitch of AudioSource #" + pId + " while it does not exists or has been released");
            }
        }

        /// <summary>
        /// Plays an audio data using an available audio source from the corresponding channel's pool.
        /// </summary>
        /// <param name="pAudioData">The audio data to play.</param>
        /// <param name="pFadeIn">Force fading in of the sound.</param>
        /// <param name="pFadeOut">Force fading out of the sound.</param>
        /// <returns>The ID of the playing audio data, used for further management.</returns>
        public long Play(AudioData pAudioData, bool pFadeIn = false, bool pFadeOut = false)
        {
            var id = Prepare(pAudioData);
            PlayPrepared(id, pFadeIn, pFadeOut);
            return id;
        }

        /// <summary>
        /// Plays an already prepared audio data, using the correspinding ID.
        /// </summary>
        /// <param name="pId">The ID returned by <see cref="Prepare"/></param>
        /// <param name="pFadeIn">Force fading in of the sound.</param>
        /// <param name="pFadeOut">Force fading out of the sound.</param>
        public void PlayPrepared(long pId, bool pFadeIn = false, bool pFadeOut = false)
        {
            if (pId < 0)
            {
                Debug.LogError("You can't give the AudioManager a negative prepared id. Did you forget to initilize it?");
                return;
            }

            var audioData = _currentlyUsedSources[pId].CurrentAudioData;
            //We override only if true
            if (audioData.m_fadeIn) {
                pFadeIn = true;
            }
            if (audioData.m_fadeOut) {
                pFadeOut = true;
            }
            _currentlyUsedSources[pId].Play(pFadeIn, pFadeOut);
        }

        /// <summary>
        /// Prepares an audio data to be played, reserving the audio source and parametrizing it.
        /// </summary>
        /// <param name="pAudioData">The audio data to prepare.</param>
        /// <returns>The ID for further management of this audio data in the AudioManager's context ; for example to play the audio data with <seealso cref="PlayPrepared"/></returns>
        public long Prepare(AudioData pAudioData)
        {
            if (pAudioData == null) {
                Debug.LogError("You can't give the AudioManager a null AudioData. Did you forget to initilize it?");
                return -1;
            }

            AudioSourceWrapper audioSource;
            try {
                audioSource = _channelsPool[pAudioData.m_channel].Get();
            } catch (Pool<AudioSourceWrapper>.PoolOverflowException) {
                Debug.LogWarning(pAudioData.m_channel + " channel's pool overflowed");
                return -1;
            }
            var id = _nextId;
            _currentlyUsedSources[id] = audioSource;
            _nextId = (_nextId + 1) % long.MaxValue;
            audioSource.OnEndOfSound +=
                src => {
                    _channelsPool[src.CurrentAudioData.m_channel].Release(src);
                    _currentlyUsedSources.Remove(id);
                    //Debug.Log("#" + id + " released sound " + src.CurrentAudioData.m_clips[0]);
                };
            audioSource.CurrentAudioData = pAudioData;
            return id;
        }

        /// <summary>
        /// Stops a curretly playing AudioData. Useful to stop looping sounds.
        /// </summary>
        /// <param name="pId">The ID of the audio data in the manager's context, returned by <seealso cref="Play"/> or <seealso cref="Prepare"/></param>
        public void Stop(long pId) {
            if (pId == -1) {
                return;
            }
            try {
                _currentlyUsedSources[pId].Stop();
            }
            catch (KeyNotFoundException) {
                Debug.LogWarning("Key " + pId + " not in dictionnary");
            }
        }

        /// <summary>
        /// Stops every currently playing audio data.
        /// </summary>
        public void StopAllAndClear()
        {
            var srcs = new List<AudioSourceWrapper>(_currentlyUsedSources.Values);
            foreach (var src in srcs)
            {
                src.Stop();
            }
            _nextId = 1;
        }

        /// <summary>
        /// Fetch the AudioSourceWrapper currently playing the AudioData affected to this ID in the manager's context.
        /// </summary>
        /// <param name="pId">The ID of the audio data in the manager's context, returned by <seealso cref="Play"/> or <seealso cref="Prepare"/></param>
        /// <returns>The AudioSourceWrapper playing the audio data.</returns>
        public AudioSourceWrapper SourcePlaying(long pId)
        {
            if (pId == -1)
            {
                return null;
            }
            try
            {
                return _currentlyUsedSources[pId];
            }
            catch (KeyNotFoundException)
            {
                Debug.LogWarning("Key " + pId + " not in dictionnary");
            }
            return null;
        }
    }
}
