﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Audio.Utils {
    public class Pool<T> {
        #region Exceptions
        public class PoolOverflowException : Exception {
        }

        public class PoolCapacityTooHighException : Exception {
        }

        public class PoolUnauthorizedObjectException : Exception {
        }
        #endregion

        private readonly Func<T> _constructor;
        private readonly int _softLimit;
        public const int HARD_LIMIT = 1000;

        private readonly Queue<T> _availableObjects;
        private readonly HashSet<T> _authorizedObjects; 
        public int Count {
            get;
            private set;
        }

        public int AvailableCount {
            get {
                return _availableObjects.Count + (_softLimit - Count);
            }
        }

        public Pool(Func<T> pConstructor, int pSoftLimit, int initialCount) {
            if (pSoftLimit > HARD_LIMIT) {
                throw new PoolCapacityTooHighException();
            }
            _constructor = pConstructor;
            _softLimit = pSoftLimit;
            _availableObjects = new Queue<T>();
            _authorizedObjects = new HashSet<T>();
            Count = 0;
            for (var i = 0; i < initialCount; ++i) {
                Create();
            }
        }

        private void Create() {
            if (Count >= _softLimit) {
                throw new PoolOverflowException();
            }
            Count++;
            var newObj = _constructor();
            _availableObjects.Enqueue(newObj);
            _authorizedObjects.Add(newObj);
        }

        public T Get() {
            if (_availableObjects.Count == 0) {
                Create();
            }
            var obj = _availableObjects.Dequeue();
            return obj;
        }

        public void Release(T obj) {
            if (!_authorizedObjects.Contains(obj)) {
                throw new PoolUnauthorizedObjectException();
            }
            _availableObjects.Enqueue(obj);
        }
    }
}
