﻿using Assets.commons.Scripts.Utils;
using UnityEngine;
using System.Collections.Generic;

namespace Audio {
    public class AudioData : UndergroundBehavior {
        #region Play type enum

        public enum PlayType {
            Sequential,
            Randomly,
            RandomlyAvoidingLast,
        }

        #endregion

        public AudioManager.Channel m_channel;
        public PlayType m_type;
        public int m_priority = 1;
		[Range(0f, 1f)]
		public float m_volume = 1f;
        [Range(-3f, 3f)]
        public float m_pitch = 1;
        public bool m_randomPitch = false;
        [Range(-3f, 3f)]
        public float m_minPitch = 1;
        [Range(-3f, 3f)]
        public float m_maxPitch = 1;
        public List<AudioClip> m_clips;
        private int _lastIndex = 0;

        public bool m_bypassEffects = false;
        public bool m_bypassReverbZones = false;
        public bool m_ignoreListenerPause = false;
        public bool m_ignoreListenerVolume = false;
        public bool m_loop = false;
        public bool m_fadeIn = false;
        public bool m_fadeOut = false;

        //3D settings
        public int m_dopplerLevel = 0;
        public AudioRolloffMode m_rolloffMode = AudioRolloffMode.Logarithmic;
        public float m_minDistance = 1f;
        [Range(0f, 1f)]
        public float m_panLevel = 0f;
        [Range(0, 360)]
        public int m_spread;
        public float m_maxDistance = 500f;
        //2D settings
        [Range(-1f, 1f)]
        public float m_pan2D = 0f;

        public bool m_playAtStart = false;

        void Start()
        {
            if (m_playAtStart)
            {
                AudioManager.Instance.Play(this);
            }
        }

        public AudioClip GetClip() {
            if (m_type == PlayType.Sequential) {
                return GetSequentialClip();
            }
            return GetRandomClip(m_type == PlayType.RandomlyAvoidingLast);
        }

        private AudioClip GetSequentialClip() {
            _lastIndex = (_lastIndex + 1) % m_clips.Count;
            return m_clips[_lastIndex];
        }

        private AudioClip GetRandomClip(bool pAvoidLastOne) {
            if (m_clips.Count == 1) {
                pAvoidLastOne = false;
            }

            var toAvoid = -1;
            if (pAvoidLastOne) {
                toAvoid = _lastIndex;
            }

            int index;
            do {
                index = Random.Range(0, m_clips.Count);
            } while (index == toAvoid);
            _lastIndex = index;
            return m_clips[index];
        }

        private float GetPitch() {
            return !m_randomPitch ? m_pitch : Random.Range(m_minPitch, m_maxPitch);
        }

        public void ConfigureAudioSource(UnityEngine.AudioSource pSrc) {
            pSrc.bypassEffects = m_bypassEffects;
            pSrc.bypassReverbZones = m_bypassReverbZones;
            pSrc.clip = GetClip();
            pSrc.dopplerLevel = m_dopplerLevel;
            pSrc.ignoreListenerPause = m_ignoreListenerPause;
            pSrc.ignoreListenerVolume = m_ignoreListenerVolume;
            pSrc.loop = m_loop;
            pSrc.maxDistance = m_maxDistance;
            pSrc.minDistance = m_minDistance;
            pSrc.pan = m_pan2D;
            pSrc.panLevel = m_panLevel;
            pSrc.pitch = GetPitch();
            pSrc.priority = m_priority;
        }
    }
}
