﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for grenade skill stats.
    /// </summary>
    public interface ISkillGrenadeStats : ISkillStats
    {
        /// <summary>
        /// Jump distance.
        /// </summary>
        int Range { get; }
    }
}