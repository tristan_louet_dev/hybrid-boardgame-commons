﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Enum for skills identifiers.
    /// </summary>
    public enum SkillID
    {
        None,
        Goat,
        Barrage,
        ProvokeJake,
        AcrobaticsAmy,
        SneakyStabRicochetAmy,
        HoloBaitAmy,
        NegligenceAmy,
        Forcefield,
        ShootDom,
        StimulantDom,
        HealingGrenadeDom,
        Explosion,
        ExplosionHeal,
        Grenade,
        Resurrect,
        ShootAI,
        ShootBaillonette
    }
}
