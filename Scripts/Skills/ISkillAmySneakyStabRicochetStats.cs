﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for amy skill stats.
    /// </summary>
    public interface ISkillAmySneakyStabRicochetStats : ISkillStats
    {
        /// <summary>
        /// Close range facing damages.
        /// </summary>
        int CloseRangeFacingDamages { get; }

        /// <summary>
        /// Close range aside damages.
        /// </summary>
        int CloseRangeAsideDamages { get; }

        /// <summary>
        /// Close range back damages.
        /// </summary>
        int CloseRangeBackDamages { get; }

        /// <summary>
        /// Long range bounces damages.
        /// </summary>
        int[] LongRangeBouncesDamages { get; }
    }
}
