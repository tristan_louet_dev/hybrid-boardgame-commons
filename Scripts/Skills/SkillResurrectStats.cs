﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Resurrect skill stats.
    /// </summary>
    [Serializable]
    public class SkillResurrectStats : SkillStats, ISkillResurrectStats
    {
        /// <summary>
        /// Number of HP recovered.
        /// </summary>
        [SerializeField]
        private int _hpRecovery;

        /// <summary>
        /// Number of AP recovered.
        /// </summary>
        [SerializeField]
        private int _apRecovery;

        /// <summary>
        /// AP limit.
        /// </summary>
        [SerializeField]
        private int _apLimit;

        #region Getters
        public int HpRecovery
        {
            get { return _hpRecovery; }
        }

        public int ApRecovery
        {
            get { return _apRecovery; }
        }

        public int ApLimit
        {
            get { return _apLimit; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillResurrectStats(ISkillResurrectStats other) : base(other)
        {
            _hpRecovery = other.HpRecovery;
            _apRecovery = other.ApRecovery;
            _apLimit = other.ApLimit;
        }
    }
}
