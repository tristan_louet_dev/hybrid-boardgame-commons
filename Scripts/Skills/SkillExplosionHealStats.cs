﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Explosion heal skill stats.
    /// </summary>
    [Serializable]
    public class SkillExplosionHealStats : SkillStats, ISkillExplosionHealStats {
        /// <summary>
        /// Heal amount.
        /// </summary>
        [SerializeField]
        private int _heal;

        #region Getters
        public int Heal
        {
            get { return _heal; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillExplosionHealStats(ISkillExplosionHealStats other) : base(other)
        {
            _heal = other.Heal;
        }
    }
}
