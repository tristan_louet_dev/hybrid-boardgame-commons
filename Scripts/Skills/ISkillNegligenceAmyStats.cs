﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for negligence skill stats.
    /// </summary>
    public interface ISkillNegligenceAmyStats : ISkillStats
    {
        /// <summary>
        /// Value the movement distance is increased by.
        /// </summary>
        int MovementAugmentation { get; }
    }
}
