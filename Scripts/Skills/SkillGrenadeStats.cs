﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Grenade skill stats.
    /// </summary>
    [Serializable]
    public class SkillGrenadeStats : SkillStats, ISkillGrenadeStats {

        /// <summary>
        /// Jump distance.
        /// </summary>
        [SerializeField]
        private int _range;

        #region Getters
        public int Range
        {
            get { return _range; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillGrenadeStats(ISkillGrenadeStats other) : base(other)
        {
            _range = other.Range;
        }
    }
}
