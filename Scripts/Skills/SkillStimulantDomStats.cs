﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Stimulant skill stats.
    /// </summary>
    [Serializable]
    public class SkillStimulantDomStats : SkillStats, ISkillStimulantDomStats
    {
        /// <summary>
        /// Heal amount.
        /// </summary>
        [SerializeField]
        private int _healAmount;

        /// <summary>
        /// Minimum heal amount.
        /// </summary>
        [SerializeField]
        private int _minHealAmount;

        /// <summary>
        /// Heal decay per tile.
        /// </summary>
        [SerializeField]
        private float _percentDecayPerTile;

        #region Getters
        public int HealAmount
        {
            get { return _healAmount; }
        }

        public int MinHealAmount
        {
            get { return _minHealAmount; }
        }

        public float PercentDecayPerTile
        {
            get { return _percentDecayPerTile; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillStimulantDomStats(ISkillStimulantDomStats other) : base(other)
        {
            _healAmount = other.HealAmount;
            _minHealAmount = other.MinHealAmount;
            _percentDecayPerTile = other.PercentDecayPerTile;
        }
    }
}
