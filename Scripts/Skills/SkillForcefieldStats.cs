﻿using System;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Forcefield skill stats.
    /// </summary>
    [Serializable]
    public class SkillForcefieldStats : SkillStats, ISkillForcefieldStats
    {
        /// <summary>
        /// Dice value to damage reduction.
        /// </summary>
        [SerializeField]
        private DiceValueFloat _buffDamageReduction;

        /// <summary>
        /// Dice value to construction lifetime (in number of turns).
        /// </summary>
        [SerializeField]
        private DiceValueInt _constructionDuration;

        #region Getters
        public DiceValueFloat BuffDamageReduction
        {
            get { return _buffDamageReduction; }
        }

        public DiceValueInt ConstructionDuration
        {
            get { return _constructionDuration; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillForcefieldStats(ISkillForcefieldStats other) : base(other)
        {
            _buffDamageReduction = other.BuffDamageReduction;
            _constructionDuration = other.ConstructionDuration;
        }
    }
}
