﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for explosion heal skill stats.
    /// </summary>
    public interface ISkillExplosionHealStats : ISkillStats
    {
        /// <summary>
        /// Heal amount.
        /// </summary>
        int Heal { get; }
    }
}
