﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Storage class for skill stats.
    /// </summary>
    [Serializable]
    public class SkillStats : ISkillStats
    {
        /// <summary>
        /// AP cost.
        /// </summary>
        [SerializeField]
        private int _apCost;

        #region Getters
        public int APCost
        {
            get { return _apCost; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillStats(ISkillStats other)
        {
            _apCost = other.APCost;
        }
    }
}
