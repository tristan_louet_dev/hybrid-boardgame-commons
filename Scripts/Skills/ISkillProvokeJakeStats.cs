﻿using Assets.commons.Scripts.Utils;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for provoke skill stats.
    /// </summary>
    public interface ISkillProvokeJakeStats : ISkillStats
    {
        /// <summary>
        /// Debuff duration.
        /// </summary>
        DiceValueInt Duration { get; }
    }
}
