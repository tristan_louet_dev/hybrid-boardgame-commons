﻿using Assets.commons.Scripts.Utils;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for holo bait skill stats.
    /// </summary>
    public interface ISkillHoloBaitAmyStats : ISkillStats
    {
        /// <summary>
        /// Hologram duration.
        /// </summary>
        DiceValueInt Duration { get; }
    }
}
