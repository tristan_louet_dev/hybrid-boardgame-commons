﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Shoot skill stats.
    /// </summary>
    [Serializable]
    public class SkillShootStats : SkillStats, ISkillShootStats
    {
        /// <summary>
        /// Damage amount.
        /// </summary>
        [SerializeField]
        private int _damage;

        #region Getters
        public int Damage
        {
            get { return _damage; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillShootStats(ISkillShootStats other) : base(other)
        {
            _damage = other.Damage;
        }
    }
}
