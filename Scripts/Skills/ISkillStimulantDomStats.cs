﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for stimulant skill stats.
    /// </summary>
    public interface ISkillStimulantDomStats : ISkillStats
    {
        /// <summary>
        /// Heal amount.
        /// </summary>
        int HealAmount { get; }

        /// <summary>
        /// Minimum heal amount.
        /// </summary>
        int MinHealAmount { get; }

        /// <summary>
        /// Heal decay per tile.
        /// </summary>
        float PercentDecayPerTile { get; }
    }
}
