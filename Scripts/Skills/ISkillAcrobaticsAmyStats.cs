﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for acrobatics skill stats.
    /// </summary>
    public interface ISkillAcrobaticsAmyStats : ISkillStats
    {
        /// <summary>
        /// Jump distance.
        /// </summary>
        int JumpDistance { get; }

        /// <summary>
        /// Damages when jumping over an unit.
        /// </summary>
        int JumpDamages { get; }
    }
}
