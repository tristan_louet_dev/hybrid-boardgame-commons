﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Goat skill stats.
    /// </summary>
    [Serializable]
    public class SkillGoatStats : SkillStats, ISkillGoatStats
    {
        /// <summary>
        /// Damages for close range.
        /// </summary>
        [SerializeField]
        private int _closeRangeDamages;

        /// <summary>
        /// Additional damages for when the target can't be pushed.
        /// </summary>
        [SerializeField]
        private int _cantPushDamages;

        /// <summary>
        /// Damages for long range.
        /// </summary>
        [SerializeField]
        private int _longRangeDamages;

        /// <summary>
        /// Number of tiles the target is pushed on.
        /// </summary>
        [SerializeField]
        private int _pushDistance;

        /// <summary>
        /// Push speed.
        /// </summary>
        [SerializeField]
        private float _pushSpeed;

        #region Getters
        public int CloseRangeDamages
        {
            get { return _closeRangeDamages; }
        }
        
        public int CantPushDamages
        {
            get { return _cantPushDamages; }
        }

        public int LongRangeDamages
        {
            get { return _longRangeDamages; }
        }

        public int PushDistance
        {
            get { return _pushDistance; }
        }

        public float PushSpeed
        {
            get { return _pushSpeed; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillGoatStats(ISkillGoatStats other)
            : base(other)
        {
            _closeRangeDamages = other.CloseRangeDamages;
            _cantPushDamages = other.CantPushDamages;
            _longRangeDamages = other.LongRangeDamages;
            _pushDistance = other.PushDistance;
            _pushSpeed = other.PushSpeed;
        }
    }
}
