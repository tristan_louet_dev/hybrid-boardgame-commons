﻿using Assets.commons.Scripts.Utils;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for forcefield skill stats.
    /// </summary>
    public interface ISkillForcefieldStats : ISkillStats
    {
        /// <summary>
        /// Dice value to damage reduction.
        /// </summary>
        DiceValueFloat BuffDamageReduction { get; }

        /// <summary>
        /// Dice value to construction lifetime (in number of turns).
        /// </summary>
        DiceValueInt ConstructionDuration { get; }
    }
}
