﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Amy skill stats.
    /// </summary>
    [Serializable]
    public class SkillAmySneakyStabRicochetStats : SkillStats, ISkillAmySneakyStabRicochetStats
    {
        /// <summary>
        /// Close range facing damages.
        /// </summary>
        [SerializeField]
        private int _closeRangeFacingDamages;

        /// <summary>
        /// Close range aside damages.
        /// </summary>
        [SerializeField]
        private int _closeRangeAsideDamages;

        /// <summary>
        /// Close range back damages.
        /// </summary>
        [SerializeField]
        private int _closeRangeBackDamages;

        /// <summary>
        /// Long range bounces damages.
        /// </summary>
        [SerializeField]
        private int[] _longRangeBouncesDamages;

        #region Getters
        public int CloseRangeFacingDamages
        {
            get { return _closeRangeFacingDamages; }
        }

        public int CloseRangeAsideDamages
        {
            get { return _closeRangeAsideDamages; }
        }

        public int CloseRangeBackDamages
        {
            get { return _closeRangeBackDamages; }
        }

        public int[] LongRangeBouncesDamages
        {
            get { return _longRangeBouncesDamages; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillAmySneakyStabRicochetStats(ISkillAmySneakyStabRicochetStats other)
            : base(other)
        {
            _closeRangeAsideDamages = other.CloseRangeAsideDamages;
            _closeRangeBackDamages = other.CloseRangeBackDamages;
            _closeRangeFacingDamages = other.CloseRangeFacingDamages;
            _longRangeBouncesDamages = other.LongRangeBouncesDamages;
        }
    }
}
