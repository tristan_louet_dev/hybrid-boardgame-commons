﻿using System;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Explosion skill stats.
    /// </summary>
    [Serializable]
    public class SkillExplosionStats : SkillShootStats, ISkillExplosionStats
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillExplosionStats(ISkillExplosionStats other) : base(other)
        {
        }
    }
}
