﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for shoot skill stats.
    /// </summary>
    public interface ISkillShootDomStats : ISkillShootStats
    {
        /// <summary>
        /// Damage decay per tile.
        /// </summary>
        float PercentDecayPerTile { get; }

        /// <summary>
        /// Minimum damage amount.
        /// </summary>
        int MinDmg { get; }

        /// <summary>
        /// Close range AP reduction.
        /// </summary>
        int CloseRangeAPReduction { get; }

        /// <summary>
        /// Long range AP reduction.
        /// </summary>
        int LongRangeAPReduction { get; }
    }
}
