﻿using System;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Provoke skill stats.
    /// </summary>
    [Serializable]
    public class SkillProvokeJakeStats : SkillStats, ISkillProvokeJakeStats
    {
        /// <summary>
        /// Debuff duration.
        /// </summary>
        [SerializeField]
        private DiceValueInt _duration;

        #region Getters
        public DiceValueInt Duration
        {
            get { return _duration; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillProvokeJakeStats(ISkillProvokeJakeStats other) : base(other)
        {
            _duration = other.Duration;
        }
    }
}
