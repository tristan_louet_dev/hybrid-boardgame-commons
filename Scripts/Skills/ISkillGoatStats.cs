﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for goat skill stats.
    /// </summary>
    public interface ISkillGoatStats : ISkillStats
    {
        /// <summary>
        /// Damages for close range.
        /// </summary>
        int CloseRangeDamages { get; }

        /// <summary>
        /// Additional damages for when the target can't be pushed.
        /// </summary>
        int CantPushDamages { get; }

        /// <summary>
        /// Damages for long range.
        /// </summary>
        int LongRangeDamages { get; }

        /// <summary>
        /// Number of tiles the target is pushed on.
        /// </summary>
        int PushDistance { get; }

        /// <summary>
        /// Push speed.
        /// </summary>
        float PushSpeed { get; }
    }
}
