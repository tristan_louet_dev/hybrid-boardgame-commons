﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for skills stats.
    /// </summary>
    public interface ISkillStats
    {
        /// <summary>
        /// Amount of AP required.
        /// </summary>
        int APCost { get; }
    }
}
