﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Acrobatics skill stats.
    /// </summary>
    [Serializable]
    public class SkillAcrobaticsAmyStats : SkillStats, ISkillAcrobaticsAmyStats
    {
        /// <summary>
        /// Jump distance.
        /// </summary>
        [SerializeField]
        private int _jumpDistance;

        /// <summary>
        /// Damages when jumping over an unit.
        /// </summary>
        [SerializeField]
        private int _jumpDamages;

        #region Getters
        public int JumpDistance
        {
            get { return _jumpDistance; }
        }

        public int JumpDamages
        {
            get { return _jumpDamages; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillAcrobaticsAmyStats(ISkillAcrobaticsAmyStats other) : base(other)
        {
            _jumpDistance = other.JumpDistance;
            _jumpDamages = other.JumpDamages;
        }
    }
}
