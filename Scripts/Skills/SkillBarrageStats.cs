﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Barrage skill stats.
    /// </summary>
    [Serializable]
    public class SkillBarrageStats : SkillStats, ISkillBarrageStats {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillBarrageStats(ISkillBarrageStats other) : base(other)
        {
            Damages = other.Damages;
        }

        [SerializeField]
        private int _damages;

        public int Damages
        {
            get { return _damages; }
            set { _damages = value; }
        }
    }
}
