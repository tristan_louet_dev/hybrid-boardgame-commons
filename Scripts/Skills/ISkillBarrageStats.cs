﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for barrage skill stats.
    /// </summary>
    public interface ISkillBarrageStats : ISkillStats
    {
        /// <summary>
        /// The damages inflicted by a single barrage shot.
        /// </summary>
        int Damages { get; }
    }
}
