﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for explosion skill stats.
    /// </summary>
    public interface ISkillExplosionStats : ISkillShootStats
    {
    }
}
