﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for shoot skill stats.
    /// </summary>
    public interface ISkillShootStats : ISkillStats
    {
        /// <summary>
        /// Damage amount.
        /// </summary>
        int Damage { get; }
    }
}
