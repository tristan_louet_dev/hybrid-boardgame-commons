﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Shoot skill stats.
    /// </summary>
    [Serializable]
    public class SkillShootDomStats : SkillShootStats, ISkillShootDomStats
    {
        /// <summary>
        /// Damage decay per tile.
        /// </summary>
        [SerializeField]
        private float _percentDecayPerTile;

        /// <summary>
        /// Minimum damage amount.
        /// </summary>
        [SerializeField]
        private int _minDmg;

        /// <summary>
        /// Close range AP reduction.
        /// </summary>
        [SerializeField]
        private int _closeRangeAPReduction;

        /// <summary>
        /// Long range AP reduction.
        /// </summary>
        [SerializeField]
        private int _longRangeAPReduction;

        #region Getters
        public float PercentDecayPerTile
        {
            get { return _percentDecayPerTile; }
        }

        public int MinDmg
        {
            get { return _minDmg; }
        }

        public int CloseRangeAPReduction
        {
            get { return _closeRangeAPReduction; }
        }

        public int LongRangeAPReduction
        {
            get { return _longRangeAPReduction; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillShootDomStats(ISkillShootDomStats other)
            : base(other)
        {
            _percentDecayPerTile = other.PercentDecayPerTile;
            _minDmg = other.MinDmg;
            _closeRangeAPReduction = other.CloseRangeAPReduction;
            _longRangeAPReduction = other.LongRangeAPReduction;
        }
    }
}
