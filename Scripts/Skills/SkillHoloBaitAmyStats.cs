﻿using System;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Holo bait skill stats.
    /// </summary>
    [Serializable]
    public class SkillHoloBaitAmyStats : SkillStats, ISkillHoloBaitAmyStats
    {
        /// <summary>
        /// Hologram duration.
        /// </summary>
        [SerializeField]
        private DiceValueInt _duration;

        #region Getters
        public DiceValueInt Duration
        {
            get { return _duration; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillHoloBaitAmyStats(ISkillHoloBaitAmyStats other) : base(other)
        {
            _duration = other.Duration;
        }
    }
}
