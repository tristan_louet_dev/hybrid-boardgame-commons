﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Negligence skill stats.
    /// </summary>
    [Serializable]
    public class SkillNegligenceAmyStats : SkillStats, ISkillNegligenceAmyStats
    {
        /// <summary>
        /// Value the movement distance is increased by.
        /// </summary>
        [SerializeField]
        private int _movementAugmentation;

        #region Getters
        public int MovementAugmentation
        {
            get { return _movementAugmentation; }
        }
        #endregion

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public SkillNegligenceAmyStats(ISkillNegligenceAmyStats other) : base(other)
        {
            _movementAugmentation = other.MovementAugmentation;
        }
    }
}
