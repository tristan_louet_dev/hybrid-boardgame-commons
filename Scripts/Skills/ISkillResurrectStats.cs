﻿namespace Assets.commons.Scripts.Skills
{
    /// <summary>
    /// Interface for resurrect skill stats.
    /// </summary>
    public interface ISkillResurrectStats : ISkillStats
    {
        /// <summary>
        /// Number of HP recovered.
        /// </summary>
        int HpRecovery { get; }

        /// <summary>
        /// Number of AP recovered.
        /// </summary>
        int ApRecovery { get; }

        /// <summary>
        /// AP limit.
        /// </summary>
        int ApLimit { get; }
    }
}
