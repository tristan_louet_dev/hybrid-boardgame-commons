﻿using System.Collections;
using UnityEngine;

namespace Assets.commons.Scripts.Utils
{
    public class DestroyAfterTimeout : UndergroundBehavior
    {
        [SerializeField]
        private float _timeout = 1f;

        void Start()
        {
            StartCoroutine(DeleteAfterTimeout());
        }

        private IEnumerator DeleteAfterTimeout()
        {
            yield return new WaitForSeconds(_timeout);
            Destroy(gameObject);
        }
    }
}
