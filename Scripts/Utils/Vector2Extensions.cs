﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Utils {
    public static class Vector2Extensions {
        public static bool Approximately(Vector2 v, Vector2 v2)
        {
            return Mathf.Approximately(v.x, v2.x) && Mathf.Approximately(v.y, v2.y);
        }

        public static float Angle(this Vector2 v, Vector2 other)
        {
            return Mathf.Atan2(v.y, v.x) - Mathf.Atan2(other.y, other.x);
        }

        public static float AngleDeg(this Vector2 v, Vector2 other)
        {
            return (v.Angle(other) * 180) / (Mathf.PI);
        }

        public static float Angle360(this Vector2 v, Vector2 other)
        {
            var angle = v.Angle(other);
            if (angle < 0)
            {
                return Mathf.PI*2 + angle;
            }
            else
            {
                return angle;
            }
        }

        public static float AngleDeg360(this Vector2 v, Vector2 other)
        {
            return (v.Angle360(other)*180)/(Mathf.PI);
        }

        public static Vector2 FromAngle(float angle)
        {
            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }
    }
}
