﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Utils {
    public static class Vector3Extensions {
        public static bool Approximately(Vector3 v, Vector3 v2)
        {
            return Mathf.Approximately(v.x, v2.x) && Mathf.Approximately(v.y, v2.y) && Mathf.Approximately(v.z, v2.z);
        }
    }
}
