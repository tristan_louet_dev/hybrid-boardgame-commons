﻿using System;

namespace Assets.commons.Scripts.Utils
{
    /// <summary>
    /// Event handler.
    /// </summary>
    /// <typeparam name="TR">Type of returned value.</typeparam>
    /// <typeparam name="TP">Type of parameter value.</typeparam>
    public interface IHandler<out TR, in TP>
    {
        TR Handle(TP value);
    }

    /// <summary>
    /// Event handler.
    /// </summary>
    /// <typeparam name="TP">Type of parameter value.</typeparam>
    public interface IHandler<in TP>
    {
        void Handle(TP value);
    }

    /// <summary>
    /// Event handler.
    /// </summary>
    public interface IHandler
    {
        void Handle();
    }

    /// <summary>
    /// Event handler.
    /// </summary>
    /// <typeparam name="TR">Type of returned value.</typeparam>
    /// <typeparam name="TP">Type of parameter value.</typeparam>
    [Serializable]
    public abstract class Handler<TR, TP> : UndergroundBehavior, IHandler<TR, TP>
    {
        public abstract TR Handle(TP value);
    }

    /// <summary>
    /// Event handler.
    /// </summary>
    /// <typeparam name="TP">Type of parameter value.</typeparam>
    [Serializable]
    public abstract class Handler<TP> : UndergroundBehavior, IHandler<TP>
    {
        public abstract void Handle(TP value);
    }

    /// <summary>
    /// Event handler.
    /// </summary>
    [Serializable]
    public abstract class Handler : UndergroundBehavior, IHandler
    {
        public abstract void Handle();
    }
}