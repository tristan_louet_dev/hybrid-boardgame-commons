﻿using UnityEngine;

namespace Assets.commons.Scripts.Utils
{
    public class UndergroundBehavior : MonoBehaviour
    {
        protected static bool IsApplicationQuitting { get; private set; }

        protected static bool IsLevelLoading { get; private set; }

        protected virtual void OnApplicationQuit()
        {
            IsApplicationQuitting = true;
        }

        public new static Object Instantiate(Object original)
        {
            return IsApplicationQuitting || IsLevelLoading ? null : Object.Instantiate(original);
        }

        public new static Object Instantiate(Object original, Vector3 position, Quaternion rotation)
        {
            return IsApplicationQuitting || IsLevelLoading ? null : Object.Instantiate(original, position, rotation);
        }

        public static void LoadLevel(string levelName)
        {
            IsLevelLoading = true;
            Application.LoadLevel(levelName);
        }

        public static void LoadLevel(int index)
        {
            IsLevelLoading = true;
            Application.LoadLevel(index);
        }

        void Awake()
        {
            IsLevelLoading = false;
        }
    }
}
