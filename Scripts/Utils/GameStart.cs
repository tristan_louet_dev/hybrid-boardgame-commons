﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Utils {
    /// <summary>
    /// This singleton allow you to set different things at game startup.
    /// </summary>
    class GameStart : Singleton<GameStart>
    {
        public override void Awake()
        {
            base.Awake();
            // We forbit the lazy fat-*ss tablet to go to sleep.
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Debug.Log("Setting sleep timeout to never -- (" + SleepTimeout.NeverSleep + ", " + Screen.sleepTimeout + ")");
        }
    }
}
