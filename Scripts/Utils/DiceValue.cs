﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Utils
{
    /// <summary>
    /// Mapping from dice to value.
    /// </summary>
    [Serializable]
    public class DiceValue<T>
    {
        /// <summary>
        /// Value 1.
        /// </summary>
        [SerializeField]
        protected T _dice1;

        /// <summary>
        /// Value 2.
        /// </summary>
        [SerializeField]
        protected T _dice2;

        /// <summary>
        /// Value 3.
        /// </summary>
        [SerializeField]
        protected T _dice3;

        /// <summary>
        /// Value 4.
        /// </summary>
        [SerializeField]
        protected T _dice4;

        /// <summary>
        /// Value 5.
        /// </summary>
        [SerializeField]
        protected T _dice5;

        /// <summary>
        /// Value 6.
        /// </summary>
        [SerializeField]
        protected T _dice6;

        public T this[int key]
        {
            get {
                switch (key)
                {
                    case 1:
                        return _dice1;
                    case 2:
                        return _dice2;
                    case 3:
                        return _dice3;
                    case 4:
                        return _dice4;
                    case 5:
                        return _dice5;
                    case 6:
                        return _dice6;
                    default:
                        Debug.LogError("impoooosiiiiblee !!!!!!");
                        return _dice6;
                }
            }
        }

        public DiceValue(T a, T b, T c, T d, T e, T f)
        {
            _dice1 = a;
            _dice2 = b;
            _dice3 = c;
            _dice4 = d;
            _dice5 = e;
            _dice6 = f;
        }
    }

    /// <summary>
    /// Associate int to dice value.
    /// </summary>
    [Serializable]
    public class DiceValueInt : DiceValue<int>
    {
        /// <summary>
        /// Default value
        /// </summary>
        public DiceValueInt() : base(1,1,1,1,1,1)
        {
        }

        
    }

    /// <summary>
    /// Associate float to dice value.
    /// </summary>
    [Serializable]
    public class DiceValueFloat : DiceValue<float>
    {
        /// <summary>
        /// Default value in inpector
        /// </summary>
        public DiceValueFloat() : base(1.00f,1.1f,1.2f,1.3f,1.4f,1.5f)
        {
        }
    }

    
}
