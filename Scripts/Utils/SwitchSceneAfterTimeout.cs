﻿using System.Collections;
using UnityEngine;

namespace Assets.commons.Scripts.Utils {
    class SwitchSceneAfterTimeout : UndergroundBehavior
    {
        /// <summary>
        /// The timeout before scene switch.
        /// </summary>
        public float mTimeout = 1f;

        /// <summary>
        /// The new scene to load.
        /// </summary>
        public string mSceneName = "default";

        /// <summary>
        /// The fadein time before switching level.
        /// </summary>
        public float mFadeoutTime = .5f;

        /// <summary>
        /// The fadeout time after switching level.
        /// </summary>
        public float mFadeinTime = .5f;

        /// <summary>
        /// The color to fade in & out.
        /// </summary>
        public Color mFadeColor = Color.black;

        void Start()
        {
            StartCoroutine(SwitchAfterTimeout());
        }

        private IEnumerator SwitchAfterTimeout()
        {
            yield return new WaitForSeconds(mTimeout);
            AutoFade.LoadLevel(mSceneName, mFadeinTime, mFadeoutTime, mFadeColor);
        }
    }
}
