﻿using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.UI {
    public class OverlayableWidget : UndergroundBehavior
    {
        private bool _overlayed = false;
        public bool Overlayed
        {
            get { return _overlayed; }
            set
            {
                _overlayed = value;
                foreach (var boxCollider in GetComponentsInChildren<BoxCollider2D>(true))
                {
                    boxCollider.enabled = !_overlayed;
                }
            }
        }
    }
}
