﻿using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.UI
{
    [RequireComponent(typeof(BoxCollider2D), typeof(UISprite))]
#if UNITY_EDITOR
    [ExecuteInEditMode]
#endif
    public class AutomaticButtonCollider : UndergroundBehavior {
        void Awake() {
            var sprite = GetComponent<UISprite>();
            var boxCollider = GetComponent<BoxCollider2D>();
            boxCollider.center = Vector3.zero;
            boxCollider.size = new Vector3(sprite.width, sprite.height, 0f);
        }

#if UNITY_EDITOR
        void Update() {
            Awake();
        }
#endif
    }
}