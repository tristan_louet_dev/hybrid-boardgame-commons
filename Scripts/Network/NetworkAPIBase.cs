﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Network
{
    public abstract class NetworkAPIBase : UndergroundBehavior
    {
        /// <summary>
        /// Serialize the given list of objects.
        /// </summary>
        /// <param name="values">Objects to serialize.</param>
        /// <returns>Serialized objects.</returns>
        protected byte[] Serialize(params object[] values)
        {
            var binFormatter = new BinaryFormatter();
            var memStream = new MemoryStream();
            var methodBase = new StackTrace().GetFrame(2).GetMethod();
            binFormatter.Serialize(memStream, methodBase.Name);
            binFormatter.Serialize(memStream, values.Count());
            for (var i = 0; i < values.Count(); ++i)
            {
                var value = values[i];
                if (value == null)
                {
                    binFormatter.Serialize(memStream, "null");
                    binFormatter.Serialize(memStream, "null");
                }
                else
                {
                    var type = value.GetType();
                    binFormatter.Serialize(memStream, values[i]);
                    binFormatter.Serialize(memStream, type.FullName);
                }
            }
            var list = memStream.ToArray();
            memStream.Close();
            return list;
        }

        /// <summary>
        /// Deserialize the given list of objects.
        /// </summary>
        /// <param name="values">Objects to deserialize.</param>
        /// <param name="method">Method to call.</param>
        /// <returns>Deserialized objects.</returns>
        protected object[] Deserialize(byte[] values, out string method)
        {
            var binFormatter = new BinaryFormatter();
            var memStream = new MemoryStream();
            memStream.Write(values, 0, values.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            method = (string)binFormatter.Deserialize(memStream);
            var size = (int)binFormatter.Deserialize(memStream);
            var list = new object[size];
            for (var i = 0; i < list.Count(); ++i)
            {
                var value = binFormatter.Deserialize(memStream);
                var typeName = (string)binFormatter.Deserialize(memStream);
                if (typeName.Equals("null"))
                {
                    list[i] = null;
                }
                else
                {
                    var type = Type.GetType(typeName);
                    list[i] = Convert.ChangeType(value, type);
                }
            }
            return list;
        }

        /// <summary>
        /// Serialize the given objects and make a RPC call server-side.
        /// </summary>
        /// <param name="values">Objects to serialize.</param>
        protected void ServerCallRPCSerialize(params object[] values)
        {
            ServerCallRPC(Serialize(values));
        }

        /// <summary>
        /// Serialize the given objects and make a RPC call client-side.
        /// </summary>
        /// <param name="values">Objects to serialize.</param>
        protected void ClientCallRPCSerialize(params object[] values)
        {
            ClientCallRPC(Serialize(values));
        }

        /// <summary>
        /// RPC call server-size.
        /// </summary>
        /// <param name="values">Serialized objects.</param>
        protected abstract void ServerCallRPC(byte[] values);

        /// <summary>
        /// RPC call client-size.
        /// </summary>
        /// <param name="values">Serialized objects.</param>
        protected abstract void ClientCallRPC(byte[] values);
    }
}
