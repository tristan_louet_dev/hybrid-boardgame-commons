﻿using Assets.commons.Scripts.Cutscenes;
using Assets.commons.Scripts.Entities;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Skills;
using Assets.commons.Scripts.Utils;

namespace Assets.commons.Scripts.Network
{
    /// <summary>
    /// The network API to handle game/companion communication.
    /// </summary>
    public interface INetworkAPI
    {
        #region Server to client
        #region Model
        #region Entities
        /// <summary>
        /// Create an entity with the given UID on companion.
        /// </summary>
        /// <param name="uid">UID.</param>
        /// <param name="stats"></param>
        void CreateEntity(int uid, IEntityStats stats);

        /// <summary>
        /// Create an enemy with the given UID on companion.
        /// </summary>
        /// <param name="uid">UID.</param>
        /// <param name="id">Enemy id.</param>
        /// <param name="stats"></param>
        void CreateEnemy(int uid, EnemyID id, IEnemyStats stats);

        /// <summary>
        /// Remove the entity with the given UID from companion.
        /// </summary>
        /// <param name="uid">UID</param>
        void DestroyEntity(int uid);

        /// <summary>
        /// Update the UID of given player.
        /// </summary>
        /// <param name="player">Player id.</param>
        /// <param name="uid">New UID.</param>
        void UpdateUID(Character player, int uid);

        /// <summary>
        /// Update stats of given player.
        /// </summary>
        /// <param name="player">Player id.</param>
        /// <param name="stats">New stats.</param>
        void UpdateStats(Character player, IPlayerStats stats);

        /// <summary>
        /// Update stats of the entity related to this UID.
        /// </summary>
        /// <param name="uid">The entity uid.</param>
        /// <param name="stats">The updated stats.</param>
        void UpdateEntityStats(int uid, IEntityStats stats);

        /// Update stats of given enemy.
        /// </summary>
        /// <param name="uid">Enemy id.</param>
        /// <param name="stats">New stats.</param>
        void UpdateEnemyStats(int uid, IEnemyStats stats);

        /// <summary>
        /// Update a skill of given player.
        /// </summary>
        /// <param name="player">Player id.</param>
        /// <param name="index">Skill index.</param>
        /// <param name="skillId">Skill id.</param>
        /// <param name="stats">New Stats.</param>
        void UpdateSkill(Character player, int index, SkillID skillId, ISkillStats stats);

        /// <summary>
        /// Update health of entity with the given UID.
        /// </summary>
        /// <param name="uid">UID.</param>
        /// <param name="value">New health value.</param>
        void UpdateHealth(int uid, int value);

        /// <summary>
        /// Update AP of entity with the given UID.
        /// </summary>
        /// <param name="uid">UID.</param>
        /// <param name="value">New AP value.</param>
        void UpdateAP(int uid, int value);
        #endregion

        /// <summary>
        /// Update the number of completed turns.
        /// </summary>
        /// <param name="value">New value.</param>
        void UpdateTurnCount(int value);

        /// <summary>
        /// Update the objective.
        /// </summary>
        /// <param name="value">New value.</param>
        void UpdateObjective(string value);

        /// <summary>
        /// Updates the score.
        /// </summary>
        /// <param name="score">The current game score.</param>
        void UpdateGameScore(int score);
        #endregion

        #region Game states
        /// <summary>
        /// Updates the client with the given gamestate.
        /// </summary>
        /// <param name="state">New gamestate value.</param>
        /// <param name="player">Player id.</param>
        void UpdateClientGameState(GameState state, Character player = Character.None);

        /// <summary>
        /// Updates the client with the given gamestate.
        /// </summary>
        /// <param name="state">New gamestate value.</param>
        /// <param name="uid">Unit id.</param>
        void UpdateClientGameStateEnemy(GameState state, int uid);

        /// <summary>
        /// Updates the client gamestate to display the game over.
        /// </summary>
        void GameOver();

        /// <summary>
        /// Request the client to send back which player wants to play next.
        /// </summary>
        /// <param name="remainingCharactersToPlay">The set of available players.</param>
        void RequestNextPlayer(CharactersSet remainingCharactersToPlay);

        /// <summary>
        /// Update the targeted entity.
        /// </summary>
        /// <param name="stats">Stats.</param>
        void UpdateTarget(IEntityStats stats);

        /// <summary>
        /// Show the enemy informations on companion.
        /// </summary>
        /// <param name="uid">Enemy UID.</param>
        void ShowEnemyInfos(int uid);

        #region PlayerTurnState
        /// <summary>
        /// When the player must choose what to do.
        /// </summary>
        void PlayerTurnChoices();

        /// <summary>
        /// When the player is moving.
        /// </summary>
        void PlayerTurnMoving();

        /// <summary>
        /// When the player is interacting.
        /// </summary>
        void PlayerTurnInteracting();

        /// <summary>
        /// When the player is using a skill.
        /// </summary>
        void PlayerTurnUsingSkill(SkillID skillId);

        /// <summary>
        /// When the player choice has been validated.
        /// </summary>
        void PlayerTurnValidated();

        /// <summary>
        /// Enable/Disable the confirm button on companion.
        /// </summary>
        /// <param name="value">Enabled.</param>
        void PlayerTurnCanConfirm(bool value);

        /// <summary>
        /// Enable/Disable the cancel button on companion.
        /// </summary>
        /// <param name="value">Enabled.</param>
        void PlayerTurnCanCancel(bool value);

        /// <summary>
        /// Enable/Disable the interaction button on companion.
        /// </summary>
        /// <param name="value">Enabled.</param>
        void PlayerTurnCanInteract(bool value);

        /// <summary>
        /// Enable/Disable the button for the given skill on companion.
        /// </summary>
        /// <param name="index">Skill index.</param>
        /// <param name="value">Enabled.</param>
        void PlayerTurnCanUseSkill(int index, bool value);

        /// <summary>
        /// Enable/Disable the roll panel.
        /// </summary>
        void PlayerTurnRoll();
        #endregion

        #region EnemyTurnState
        /// <summary>
        /// Show the "Next enemy" message on companion.
        /// </summary>
        void NextEnemyTurn();
        #endregion

        #region Cutscene
        /// <summary>
        /// Play the given cutscene.
        /// </summary>
        /// <param name="cutscene">Cutscene to play.</param>
        /// <param name="parameters">Parameters.</param>
        void PlayCutscene(Cutscene cutscene, ICutsceneParams parameters);
        #endregion
        #endregion

        #region Dice
        /// <summary>
        /// Request the next roll value.
        /// </summary>
        void RequestRollValue();

        /// <summary>
        /// Send the defaultPlayerDamageMultiplicator.
        /// </summary>
        /// <param name="value">New value.</param>
        void DefaultPlayerDamageMultiplicator(DiceValueFloat value);
        #endregion

        #region Mute
        /// <summary>
        /// Called when the music has been muted.
        /// </summary>
        /// <param name="muted"></param>
        void MusicMuted(bool muted);

        /// <summary>
        /// Called when the sound has been muted.
        /// </summary>
        /// <param name="muted"></param>
        void SoundMuted(bool muted);
        #endregion
        #endregion

        #region Client to server
        /// <summary>
        /// Updates the Server with the given gamestate.
        /// </summary>
        /// <param name="state">New gamestate value.</param>
        void UpdateServerGameState(GameState state);

        /// <summary>
        /// Notify the server that the player validated the pre game screen.
        /// </summary>
        void PreGameConfirmation();

        /// <summary>
        /// Tells the server to play this level.
        /// </summary>
        /// <param name="level">The name of the level to be loaded.</param>
        void LaunchLevel(string level);

        /// <summary>
        /// When the companion is ready to receive RPC, i.e. after the game state changed.
        /// </summary>
        void CompanionReady();

        /// <summary>
        /// When the players decided which character will play next.
        /// </summary>
        /// <param name="character">The character playing next.</param>
        void NextPlayerChosen(Character character);

        /// <summary>
        /// When the player decides to use an active skill.
        /// </summary>
        /// <param name="index">Skill index.</param>
        void ActionActiveSkill(int index);

        /// <summary>
        /// When the player decides to interact with the scenery.
        /// </summary>
        void ActionInteract();

        /// <summary>
        /// Toggle interactables icons on or off.
        /// </summary>
        /// <param name="value"></param>
        void ActionToggleInteractables(bool value);

        /// <summary>
        /// When the player confirms its action.
        /// </summary>
        void ActionConfirm();

        /// <summary>
        /// When the player decides to cancel its action.
        /// </summary>
        void ActionCancel();

        /// <summary>
        /// When the player decides to end its turn.
        /// </summary>
        void ActionEndTurn();

        /// <summary>
        /// Activate or deactivate the skillshot.
        /// </summary>
        /// <param name="activated">Whether or not the skillshot is activated.</param>
        /// <param name="feedbackDistance">The feedback distance without facebook.</param>
        void ActivateSkillShot(bool activated, float feedbackDistance);

        void PlayPawnWithOneFinger(bool oneFinger);

        #region Cutscene
        /// <summary>
        /// When the cutscene has been played.
        /// </summary>
        void CutscenePlayed();
        #endregion

        #region Dice
        /// <summary>
        /// When the dice has been rolled.
        /// </summary>
        /// <param name="value">Roll value.</param>
        void RollValue(int value);
        #endregion

        #region Mute
        /// <summary>
        /// Mute the music.
        /// </summary>
        /// <param name="mute"></param>
        void MuteMusic(bool mute);

        /// <summary>
        /// Mute the sound.
        /// </summary>
        /// <param name="mute"></param>
        void MuteSound(bool mute);
        #endregion
        #endregion
    }
}
