﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for unit stats.
    /// </summary>
    public interface IUnitStats : IEntityStats
    {
        /// <summary>
        /// Get the maximum AP value.
        /// </summary>
        /// <returns>The maximum AP value.</returns>
        int APMax { get; }

        /// <summary>
        /// Get the current AP value.
        /// </summary>
        /// <returns>The current AP value.</returns>
        int APCurrent { get; }

        /// <summary>
        /// Get the maximum movement distance.
        /// </summary>
        /// <returns>The maximum movement distance.</returns>
        int MovementMax { get; }

        /// <summary>
        /// Get the movement reduction.
        /// </summary>
        int MovementReduction { get; set; }

        /// <summary>
        /// Movement distance.
        /// </summary>
        int Movement { get; }

        /// <summary>
        /// Get the AP reduction.
        /// </summary>
        int APReduction { get; set; }

        /// <summary>
        /// Get the AP limit.
        /// </summary>
        int APLimit { get; set; }

        /// <summary>
        /// Get the damage reduction.
        /// </summary>
        float DamageReduction { get; set; }

        float SuppressionDamageReductionPercent { get; set; }

        /// <summary>
        /// Returns this entity status flag.
        /// </summary>
        Status Status { get; set; }
    }
}
