﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for units.
    /// </summary>
    public interface IUnit : IEntity
    {
        /// <summary>
        /// Get the unit stats.
        /// </summary>
        new IUnitStats Stats { get; set; }
    }
}
