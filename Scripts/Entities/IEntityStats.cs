﻿using System.Collections.Generic;
using Assets.commons.Scripts.Buffs;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for entity stats.
    /// </summary>
    public interface IEntityStats
    {
        /// <summary>
        /// Get/Set the maximum HP value.
        /// </summary>
        int HPMax { get; }

        /// <summary>
        /// Get/Set the current HP value.
        /// </summary>
        int HPCurrent { get; }

        /// <summary>
        /// The current applied buff on this entity.
        /// </summary>
        IList<IBuffStats> Buffs { get; }
    }
}
