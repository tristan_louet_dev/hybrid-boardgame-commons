﻿using System;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Unit stats.
    /// </summary>
    [Serializable]
    public class UnitStats : EntityStats, IUnitStats
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public UnitStats(IUnitStats other) : base(other)
        {
            APMax = other.APMax;
            APCurrent = other.APCurrent;
            MovementMax = other.MovementMax;
            MovementReduction = other.MovementReduction;
            Movement = other.Movement;
            APReduction = other.APReduction;
            APLimit = other.APLimit;
            DamageReduction = other.DamageReduction;
            SuppressionDamageReductionPercent = other.SuppressionDamageReductionPercent;
            Status = other.Status;
        }

        public int APMax { get; private set; }

        public int APCurrent { get; set; }

        public int MovementMax { get; private set; }

        public int MovementReduction { get; set; }

        public int Movement { get; private set; }

        public int APReduction { get; set; }

        public int APLimit { get; set; }

        public float DamageReduction { get; set; }

        public Status Status { get; set; }

        public float SuppressionDamageReductionPercent { get; set; }
    }
}
