﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for enemy stats.
    /// </summary>
    public interface IEnemyStats : IUnitStats
    {
    }
}