﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Enemy stats.
    /// </summary>
    public class MutableEnemyStats : MutableUnitStats, IEnemyStats
    {
        public void Init(IEnemyStats other)
        {
            base.Init(other);
        }
    }
}
