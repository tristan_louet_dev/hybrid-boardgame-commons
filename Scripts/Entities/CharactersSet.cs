﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Assets.commons.Scripts.Entities {
    [Serializable]
    public class CharactersSet : HashSet<Character>
    {
        public CharactersSet(SerializationInfo info, StreamingContext context)
            : base(info.GetValue("DataArray", typeof(Character[])) as Character[])
        {}

        public CharactersSet(IEnumerable<Character> collection)
            : base(collection)
        {}

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("DataArray", this.ToArray());
        }
    }
}
