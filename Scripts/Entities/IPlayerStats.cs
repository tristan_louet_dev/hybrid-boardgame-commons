﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for player stats.
    /// </summary>
    public interface IPlayerStats : IUnitStats
    {
        /// <summary>
        /// Indicate if the player is lying down => 0 HP.
        /// </summary>
        bool IsDown { get; }

        /// <summary>
        /// Number of times the player has been marked as down.
        /// </summary>
        int DownCount { get; }

        /// <summary>
        /// Number of turns since the player has been marked as down.
        /// </summary>
        int DownDuration { get; set; }
    }
}
