﻿using System;

namespace Assets.commons.Scripts.Entities {
    [Flags]
    [Serializable]
    public enum Status {
        None = 1,
        Burning = 2,
        Electrified = 4,
        Shielded = 8,
        Suppressed = 16,
        InCover = 32,
        Down = 64,
        Dead = 128,
        Provoked = 256,
        Cloaked = 512,
    }

    /// <summary>
    /// Useful Status management methods to abstract internal flags implementation.
    /// </summary>
    public static class StatusExtensions
    {
        /// <summary>
        /// Adds the newStatus state to this status.
        /// </summary>
        /// <param name="self">This status.</param>
        /// <param name="newStatus">The new information.</param>
        /// <remarks>Remember to store the new value, as Status is unmutable.</remarks>
        /// <returns>The updated status.</returns>
        public static Status Add(this Status self, Status newStatus)
        {
            return self | newStatus;
        }

        /// <summary>
        /// Removes the newStatus state to this status.
        /// </summary>
        /// <param name="self">This status.</param>
        /// <param name="oldStatus">The old information.</param>
        /// <remarks>Remember to store the new value, as Status is unmutable.</remarks>
        /// <returns>The updated status.</returns>
        public static Status Remove(this Status self, Status oldStatus)
        {
            return self & ~oldStatus;
        }

        /// <summary>
        /// Indicates wether or not this status is in this state.
        /// </summary>
        /// <param name="self">This status.</param>
        /// <param name="requiredStatus">The information to check against this Status.</param>
        /// <returns>If this status is in the required state.</returns>
        public static bool Is(this Status self, Status requiredStatus)
        {
            return (self & requiredStatus) == requiredStatus;
        }
    }
}
