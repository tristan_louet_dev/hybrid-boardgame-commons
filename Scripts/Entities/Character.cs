﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Enum for playable characters.
    /// </summary>
    public enum Character
    {
        None,
        Jake,
        Amy,
        Dom,
        Hologram,
    }
}