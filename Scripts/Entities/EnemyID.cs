﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Enum for enemies identifiers.
    /// </summary>
    public enum EnemyID
    {
        AssaultRifle,
        Shotgun,
        D20N3,
        M45S70Ck
    }
}
