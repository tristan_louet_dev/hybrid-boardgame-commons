﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for players.
    /// </summary>
    public interface IPlayer : IUnit
    {
        /// <summary>
        /// Get the player id.
        /// </summary>
        Character Id { get; }

        /// <summary>
        /// Get the player stats.
        /// </summary>
        new IPlayerStats Stats { get; set; }
    }
}
