﻿using System;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Player stats.
    /// </summary>
    [Serializable]
    public class PlayerStats : UnitStats, IPlayerStats
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public PlayerStats(IPlayerStats other) : base(other)
        {
            IsDown = other.IsDown;
            DownCount = other.DownCount;
            DownDuration = other.DownDuration;
        }

        public bool IsDown { get; private set; }

        public int DownCount { get; private set; }

        public int DownDuration { get; set; }
    }
}
