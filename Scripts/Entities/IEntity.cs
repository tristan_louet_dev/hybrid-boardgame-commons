﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for entities.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Get the unique identifier of this entity.
        /// </summary>
        int UID { get; }

        /// <summary>
        /// Get the entity stats.
        /// </summary>
        IEntityStats Stats { get; set; }

        /// <summary>
        /// Indicate if the entity can take damages.
        /// </summary>
        bool IsDestroyable { get; }

        #region IsUnit, IsPlayer, IsEnemy
        /// <summary>
        /// Indicate if it is an unity.
        /// </summary>
        bool IsUnit { get; }

        /// <summary>
        /// Indicate if it is a player.
        /// </summary>
        bool IsPlayer { get; }

        /// <summary>
        /// Indicate if it is an enemy.
        /// </summary>
        bool IsEnemy { get; }

        /// <summary>
        /// Get this entity as a player.
        /// </summary>
        IPlayer AsPlayer { get; }

        /// <summary>
        /// Get this entity as an enemy.
        /// </summary>
        IEnemy AsEnemy { get; }

        /// <summary>
        /// Get this entity as an unit.
        /// </summary>
        IUnit AsUnit { get; }
        #endregion
    }
}
