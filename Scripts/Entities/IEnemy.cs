﻿namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Interface for enemies.
    /// </summary>
    public interface IEnemy : IUnit
    {
        /// <summary>
        /// Get the enemy id.
        /// </summary>
        EnemyID Id { get; }

        /// <summary>
        /// Get the enemy stats.
        /// </summary>
        new IEnemyStats Stats { get; set; }
    }
}
