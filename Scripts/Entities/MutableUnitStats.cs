﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Unit stats.
    /// </summary>
    public class MutableUnitStats : MutableEntityStats, IUnitStats
    {
        /// <summary>
        /// Maximum AP value.
        /// </summary>
        [SerializeField]
        private int _apMax;

        /// <summary>
        /// Get/Set the maximum AP value.
        /// </summary>
        public int APMax
        {
            get { return _apMax; }
            set
            {
                _apMax = Math.Max(0, value);
                APCurrent = APCurrent;
            }
        }

        /// <summary>
        /// Current AP value.
        /// </summary>
        [SerializeField]
        private int _apCurrent;

        /// <summary>
        /// Get/Set the current AP value.
        /// </summary>
        public int APCurrent
        {
            get { return _apCurrent; }
            set
            {
                _apCurrent = Mathf.Clamp(value, 0, _apMax);
            }
        }

        /// <summary>
        /// Get/Set the maximum movement distance.
        /// </summary>
        public int MovementMax
        {
            get { return Movement - MovementReduction; }
        }

        /// <summary>
        /// Movement reduction.
        /// </summary>
        [SerializeField]
        private int _movementReduction;

        /// <summary>
        /// Get/Set the movement reduction.
        /// </summary>
        public int MovementReduction
        {
            get { return _movementReduction; }
            set { _movementReduction = value; }
        }

        /// <summary>
        /// Movement distance.
        /// </summary>
        [SerializeField]
        private int _movement;

        /// <summary>
        /// Get the movement distance.
        /// </summary>
        public int Movement
        {
            get { return _movement; }
            set { _movement = value; }
        }

        /// <summary>
        /// AP reduction.
        /// </summary>
        [SerializeField]
        private int _apReduction;

        /// <summary>
        /// Get/Set the AP reduction.
        /// </summary>
        public int APReduction
        {
            get { return _apReduction; }
            set
            {
                _apReduction = value;
            }
        }

        /// <summary>
        /// AP limit.
        /// </summary>
        [SerializeField]
        private int _apLimit;

        /// <summary>
        /// Get/Set the AP limit.
        /// </summary>
        public int APLimit
        {
            get { return _apLimit; }
            set
            {
                _apLimit = value;
            }
        }

        /// <summary>
        /// Damage reduction.
        /// </summary>
        [SerializeField]
        private float _damageReduction;

        /// <summary>
        /// Get/Set the damage reduction.
        /// </summary>
        public float DamageReduction
        {
            get { return _damageReduction; }
            set
            {
                _damageReduction = value;
            }
        }

        [SerializeField]
        private float _suppressionDamageReductionPercent;

        public float SuppressionDamageReductionPercent
        {
            get { return _suppressionDamageReductionPercent; }
            set
            {
                _suppressionDamageReductionPercent = value;
            }
        }

        public Status Status { get; set; }

        public void Init(IUnitStats other)
        {
            base.Init(other);
            APMax = other.APMax;
            APCurrent = other.APCurrent;
            MovementReduction = other.MovementReduction;
            Movement = other.Movement;
            APReduction = other.APReduction;
            APLimit = other.APLimit;
            DamageReduction = other.DamageReduction;
            Status = other.Status;
            SuppressionDamageReductionPercent = other.SuppressionDamageReductionPercent;
        }
    }
}
