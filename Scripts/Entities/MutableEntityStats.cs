﻿using System;
using System.Collections.Generic;
using Assets.commons.Scripts.Buffs;
using Assets.commons.Scripts.Utils;
using UnityEngine;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Entity stats.
    /// </summary>
    public class MutableEntityStats : UndergroundBehavior, IEntityStats
    {
        public Event<IEntityStats> mOnHPChanges = new Event<IEntityStats>();
  
        /// <summary>
        /// Maximum HP value.
        /// </summary>
        [SerializeField]
        private int _hpMax;

        /// <summary>
        /// Get/Set the maximum HP value.
        /// </summary>
        public int HPMax
        {
            get { return _hpMax; }
            set
            {
                _hpMax = Math.Max(0, value);
                HPCurrent = HPCurrent;
            }
        }

        /// <summary>
        /// Current HP value.
        /// </summary>
        [SerializeField]
        private int _hpCurrent;

        /// <summary>
        /// Get/Set the current HP value.
        /// </summary>
        public int HPCurrent
        {
            get { return _hpCurrent; }
            set
            {
                _hpCurrent = Mathf.Clamp(value, 0, _hpMax);
                mOnHPChanges.Fire(this);
            }
        }

        public IList<IBuffStats> Buffs { get; private set; }

        public void Init(IEntityStats other)
        {
            HPMax = other.HPMax;
            HPCurrent = other.HPCurrent;
            Buffs = new List<IBuffStats>(other.Buffs);
            mOnHPChanges.Fire(this);
        }

        void Awake()
        {
            Buffs = new List<IBuffStats>();
        }
    }
}
