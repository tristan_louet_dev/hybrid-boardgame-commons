﻿using System;
using System.Collections.Generic;
using Assets.commons.Scripts.Buffs;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Entity stats.
    /// </summary>
    [Serializable]
    public class EntityStats : IEntityStats
    {
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="other">Other stats.</param>
        public EntityStats(IEntityStats other)
        {
            HPMax = other.HPMax;
            HPCurrent = other.HPCurrent;
            Buffs = new List<IBuffStats>(other.Buffs);
        }

        public int HPMax { get; private set; }

        public int HPCurrent { get; private set; }

        public IList<IBuffStats> Buffs { get; private set; }
    }
}
