﻿using System;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Enemy stats.
    /// </summary>
    [Serializable]
    public class EnemyStats : UnitStats, IEnemyStats
    {
        public EnemyStats(IEnemyStats other) : base(other)
        {
        }
    }
}
