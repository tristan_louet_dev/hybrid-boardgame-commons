﻿using System;
using UnityEngine;

namespace Assets.commons.Scripts.Entities
{
    /// <summary>
    /// Player stats.
    /// </summary>
    public class MutablePlayerStats : MutableUnitStats, IPlayerStats
    {
        /// <summary>
        /// Indicate if the player is marked as down.
        /// </summary>
        [SerializeField]
        private bool _isDown;

        /// <summary>
        /// Get/Set wether the player is marked as down or not.
        /// </summary>
        public bool IsDown
        {
            get { return _isDown; }
            set { _isDown = value; }
        }

        /// <summary>
        /// Number of times the player has been marked as down.
        /// </summary>
        [SerializeField]
        private int _downCount;

        /// <summary>
        /// Get/Set the number of times the player has been marked as down.
        /// </summary>
        public int DownCount
        {
            get { return _downCount; }
            set { _downCount = value; }
        }

        /// <summary>
        /// Number of turns since the player has been marked as down.
        /// </summary>
        [SerializeField]
        private int _downDuration;

        /// <summary>
        /// Get/Set the number of turns since the player has been marked as down.
        /// </summary>
        public int DownDuration
        {
            get { return _downDuration; }
            set { _downDuration = value; }
        }



        public void Init(IPlayerStats other)
        {
            base.Init(other);
            IsDown = other.IsDown;
            DownCount = other.DownCount;
            DownDuration = other.DownDuration;
            SuppressionDamageReductionPercent = other.SuppressionDamageReductionPercent;

        }
    }
}
